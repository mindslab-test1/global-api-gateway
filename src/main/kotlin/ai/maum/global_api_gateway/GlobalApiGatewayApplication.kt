package ai.maum.global_api_gateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GlobalApiGatewayApplication

fun main(args: Array<String>) {
    runApplication<GlobalApiGatewayApplication>(*args)
}
